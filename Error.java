package maven_git;

public enum Error {
	  INVALID_URl(101, "Invalid URL"),
	  INVALID_LOCALREPO(102, "Destination path already exists"),
	  INVALID_PATH(103,"Reinitialized existing Git repository"),
	  INVALID_USER(104, "Invalid user"),
	 INVALID_REPO(105,"Invalid Initial "),
	 URI_NOTSUPPORTED(106,"URI not Supported"),
	 INVALID_PUSH(107,"Commit first"),
	 INVALID_GIT_REPO(104, "Repository not found or it is not git directory");

	  private final int id;
	  private final String msg;

	  Error(int id, String msg) {
	    this.id = id;
	    this.msg = msg;
	  }

	  public int getId() {
	    return this.id;
	  }

	  public String getMsg() {
	    return this.msg;
	  }
	  public String toString() {
		    return id + ": " + msg;
	  }
	 }