package maven_git;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.RepositoryNotFoundException;

public class Commit {
	
	final static Logger logger=Logger.getLogger(Commit.class);
	
	public Response CommitFile (String gitdirectory,  String msg) throws IOException, GitAPIException, URISyntaxException
	{
          Git git = null;
       
  try {
	    PropertyConfigurator.configure("log4j.properties");
		BasicConfigurator.configure();
      	git = Git.open(new File(gitdirectory));
	 	/*RemoteAddCommand remoteAddCommand = git.remoteAdd();
	 	remoteAddCommand.setName("origin");
		remoteAddCommand.setUri(new URIish(repoUrl));
		remoteAddCommand.call();
		*/
        git.add().addFilepattern(".").call();
     
        
	    git.commit().setMessage(msg).call();
	    logger.info(Success.VALID_COMMIT);
        return new Response(Success.VALID_COMMIT);
        
  	} catch (RepositoryNotFoundException rne) {
  		logger.error(Error.INVALID_GIT_REPO);
  		return new Response(Error.INVALID_GIT_REPO);
  	}    
 
}

}
