package maven_git;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;





public class Push {
	
	
	final static Logger logger=Logger.getLogger(Push.class);
	
	public Response pushRepo ( String gitdirectory, String repoUrl, String username, String password) throws  GitAPIException, URISyntaxException, IOException
	{
          
		Git git = null;
  try {
	
	    PropertyConfigurator.configure("log4j.properties");
		BasicConfigurator.configure();
      	/*git = Git.open(new File(gitdirectory));
	 	RemoteAddCommand remoteAddCommand = git.remoteAdd();
	 	remoteAddCommand.setName("origin");
		remoteAddCommand.setUri(new URIish(repoUrl));
		remoteAddCommand.call();
		
        git.add().addFilepattern(".").call();
        String msg = "committed";
        
	    git.commit().setMessage(msg).call();
	    logger.info(Success.VALID_COMMIT);*/
//	  Repository repo=new 
//     Git git =new Git(repo);
      
		
		
		git = Git.open(new File(gitdirectory));
		PushCommand pushCommand = git.push();
        pushCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password));
        pushCommand.setRemote(repoUrl).add("master").call();
        logger.info(Success.VALID_PUSH);
        return new Response(Success.VALID_PUSH);
        
  	} catch (RepositoryNotFoundException rne) {
   	  
  		logger.error(Error.INVALID_GIT_REPO);
  		return new Response(Error.INVALID_GIT_REPO);
  	}  catch (TransportException ue) {

  		if(ue.getMessage().contains("not authorized")) {
  			logger.error(Error.INVALID_USER);
  			return new Response(Error.INVALID_USER);
  		}else {
  			logger.error(Error.INVALID_URl);
  			return new Response(Error.INVALID_URl);
  			
  		}	
  	
	}  catch(JGitInternalException je) {
		logger.error(Error.INVALID_PUSH);
		return new Response(Error.INVALID_PUSH);
	}
 
}


}
