package maven_git;

import java.nio.file.Paths;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;

public class Init {
	final static Logger logger=Logger.getLogger(Init.class);
	//String initDirectoryPath="G:\\tmp\\test1";

	public Response InitRepo(String initDirectoryPath)throws GitAPIException{
				try
		{
		PropertyConfigurator.configure("log4j.properties");
		BasicConfigurator.configure();	
			
	   	Git.init()
		.setDirectory(Paths.get(initDirectoryPath).toFile())
        .call();
		
		logger.info(Success.SCCESSFUL_INIT);
	    return new Response(Success.SCCESSFUL_INIT);
		}
		/*catch(Exception e) {
			System.out.println(e);*/
		catch(JGitInternalException jge) {
			logger.error(Error.INVALID_PATH);
    		return new Response(Error.INVALID_PATH);
		}
				//return null;
	}

	
}

